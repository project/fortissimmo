<?php

/**
 * @file
 * Provides default Views implementations for Fortissimmo accounts
 */

/**
 * Implementation of hook_views_default_views().
 */
function fortissimmo_views_default_views() {
  $views = array();
  $accounts = fortissimmo_get_accounts();

  if (!$accounts) {
    return;
  }

  foreach ($accounts as $accountid => $accountname) {
    /*
     * View 'fortissimmo_<accountid>_property_detail'
     */
    $detail_view = new view();
    $detail_view->name = 'fortissimmo_' . $accountid . '_property_detail';
    $detail_view->description = 'Default output for a Fortissimmo property detail page for account ' . check_plain($accountname);
    $detail_view->tag = 'fortissimmo';
    $detail_view->base_table = $detail_view->base_table;
    $detail_view->human_name = 'Fortissimmo Detail';
    $detail_view->core = 7;
    $detail_view->api_version = '3.0';
    $detail_view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Defaults */
    $detail_handler = $detail_view->new_display('default', 'Defaults', 'default');
    $detail_handler->display->display_options['use_more_always'] = FALSE;
    $detail_handler->display->display_options['access']['type'] = 'none';
    $detail_handler->display->display_options['cache']['type'] = 'none';
    $detail_handler->display->display_options['query']['type'] = 'views_query';
    $detail_handler->display->display_options['exposed_form']['type'] = 'basic';
    $detail_handler->display->display_options['pager']['type'] = 'some';
    $detail_handler->display->display_options['pager']['options']['items_per_page'] = '1';
    $detail_handler->display->display_options['pager']['options']['offset'] = '0';
    $detail_handler->display->display_options['style_plugin'] = 'default';
    $detail_handler->display->display_options['row_plugin'] = 'fields';
    /* Field: Fortissimmo Property: Title */
    $detail_handler->display->display_options['fields']['title']['id'] = 'title';
    $detail_handler->display->display_options['fields']['title']['table'] = $detail_view->base_table;
    $detail_handler->display->display_options['fields']['title']['field'] = 'title';
    $detail_handler->display->display_options['fields']['title']['label'] = '';
    $detail_handler->display->display_options['fields']['title']['fortissimmo_title_make_link'] = FALSE;
    $detail_handler->display->display_options['fields']['title']['fortissimmo_title_reference_fallback'] = TRUE;
    /* Field: Fortissimmo Property: Description */
    $detail_handler->display->display_options['fields']['description']['id'] = 'description';
    $detail_handler->display->display_options['fields']['description']['table'] = $detail_view->base_table;
    $detail_handler->display->display_options['fields']['description']['field'] = 'description';
    $detail_handler->display->display_options['fields']['description']['hide_empty'] = TRUE;
    $detail_handler->display->display_options['fields']['description']['description_input_format'] = 'full_html';
    /* Field: Fortissimmo Property: Reference */
    $detail_handler->display->display_options['fields']['reference']['id'] = 'reference';
    $detail_handler->display->display_options['fields']['reference']['table'] = $detail_view->base_table;
    $detail_handler->display->display_options['fields']['reference']['field'] = 'reference';
    $detail_handler->display->display_options['fields']['reference']['hide_empty'] = TRUE;
    /* Contextual filter: Fortissimmo Property: Fortissimmo ID */
    $detail_handler->display->display_options['arguments']['fortissimmo']['id'] = 'fortissimmo';
    $detail_handler->display->display_options['arguments']['fortissimmo']['table'] = $detail_view->base_table;
    $detail_handler->display->display_options['arguments']['fortissimmo']['field'] = 'fortissimmo';
    $detail_handler->display->display_options['arguments']['fortissimmo']['default_action'] = 'not found';
    $detail_handler->display->display_options['arguments']['fortissimmo']['exception']['title_enable'] = TRUE;
    $detail_handler->display->display_options['arguments']['fortissimmo']['exception']['title'] = 'Alle';
    $detail_handler->display->display_options['arguments']['fortissimmo']['title_enable'] = TRUE;
    $detail_handler->display->display_options['arguments']['fortissimmo']['title'] = '%1';
    $detail_handler->display->display_options['arguments']['fortissimmo']['breadcrumb_enable'] = TRUE;
    $detail_handler->display->display_options['arguments']['fortissimmo']['breadcrumb'] = '%1';
    $detail_handler->display->display_options['arguments']['fortissimmo']['default_argument_type'] = 'fixed';
    $detail_handler->display->display_options['arguments']['fortissimmo']['summary']['number_of_records'] = '0';
    $detail_handler->display->display_options['arguments']['fortissimmo']['summary']['format'] = 'default_summary';
    $detail_handler->display->display_options['arguments']['fortissimmo']['summary_options']['items_per_page'] = '25';
    $detail_handler->display->display_options['arguments']['fortissimmo']['specify_validation'] = TRUE;
    $translatables['fortissimmo_' . $accountid . '_property_detail'] = array(
      t('Defaults'),
      t('more'),
      t('Apply'),
      t('Reset'),
      t('Sort by'),
      t('Asc'),
      t('Desc'),
      t('Description'),
      t('Reference'),
      t('Alle'),
      t('%1'),
    );

    $views[$detail_view->name] = $detail_view;

    /*
     * View 'fortissimmo_<accountid>_overview'
     */
    $overview_view = new view();
    $overview_view->name = 'fortissimmo' . $accountid . '_overview';
    $overview_view->description = '';
    $overview_view->tag = 'default';
    $overview_view->base_table = 'tbl' . $accountid . '_pand';
    $overview_view->human_name = 'Fortissimmo Overview';
    $overview_view->core = 7;
    $overview_view->api_version = '3.0';
    $overview_view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Master */
    $overview_handler = $overview_view->new_display('default', 'Master', 'default');
    $overview_handler->display->display_options['title'] = 'Fortissimmo';
    $overview_handler->display->display_options['use_more_always'] = FALSE;
    $overview_handler->display->display_options['access']['type'] = 'none';
    $overview_handler->display->display_options['cache']['type'] = 'none';
    $overview_handler->display->display_options['query']['type'] = 'views_query';
    $overview_handler->display->display_options['exposed_form']['type'] = 'basic';
    $overview_handler->display->display_options['pager']['type'] = 'none';
    $overview_handler->display->display_options['style_plugin'] = 'default';
    $overview_handler->display->display_options['row_plugin'] = 'fields';
    /* Field: Fortissimmo Property: Fortissimmo ID */
    $overview_handler->display->display_options['fields']['fortissimmo']['id'] = 'fortissimmo';
    $overview_handler->display->display_options['fields']['fortissimmo']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['fortissimmo']['field'] = 'fortissimmo';
    /* Field: Fortissimmo Property: Address: Box */
    $overview_handler->display->display_options['fields']['box']['id'] = 'box';
    $overview_handler->display->display_options['fields']['box']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['box']['field'] = 'box';
    /* Field: Fortissimmo Property: Address: City */
    $overview_handler->display->display_options['fields']['city']['id'] = 'city';
    $overview_handler->display->display_options['fields']['city']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['city']['field'] = 'city';
    $overview_handler->display->display_options['fields']['city']['use_access_control'] = 1;
    /* Field: Fortissimmo Property: Address: City alias */
    $overview_handler->display->display_options['fields']['cityValue_Alias']['id'] = 'cityValue_Alias';
    $overview_handler->display->display_options['fields']['cityValue_Alias']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['cityValue_Alias']['field'] = 'cityValue_Alias';
    /* Field: Fortissimmo Property: Address: Country */
    $overview_handler->display->display_options['fields']['countryValue']['id'] = 'countryValue';
    $overview_handler->display->display_options['fields']['countryValue']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['countryValue']['field'] = 'countryValue';
    /* Field: Fortissimmo Property: Address: Number */
    $overview_handler->display->display_options['fields']['number']['id'] = 'number';
    $overview_handler->display->display_options['fields']['number']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['number']['field'] = 'number';
    /* Field: Fortissimmo Property: Address: Postal code */
    $overview_handler->display->display_options['fields']['postal_code']['id'] = 'postal_code';
    $overview_handler->display->display_options['fields']['postal_code']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['postal_code']['field'] = 'postal_code';
    /* Field: Fortissimmo Property: Address: Street */
    $overview_handler->display->display_options['fields']['street']['id'] = 'street';
    $overview_handler->display->display_options['fields']['street']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['street']['field'] = 'street';
    /* Field: Fortissimmo Property: Already build surface */
    $overview_handler->display->display_options['fields']['area_build']['id'] = 'area_build';
    $overview_handler->display->display_options['fields']['area_build']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['area_build']['field'] = 'area_build';
    $overview_handler->display->display_options['fields']['area_build']['precision'] = '0';
    /* Field: Fortissimmo Property: Availability */
    $overview_handler->display->display_options['fields']['available']['id'] = 'available';
    $overview_handler->display->display_options['fields']['available']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['available']['field'] = 'available';
    /* Field: Fortissimmo Property: Building Name */
    $overview_handler->display->display_options['fields']['Building_Name']['id'] = 'Building_Name';
    $overview_handler->display->display_options['fields']['Building_Name']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['Building_Name']['field'] = 'Building_Name';
    /* Field: Fortissimmo Property: Building type */
    $overview_handler->display->display_options['fields']['building_typevalue']['id'] = 'building_typevalue';
    $overview_handler->display->display_options['fields']['building_typevalue']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['building_typevalue']['field'] = 'building_typevalue';
    /* Field: Fortissimmo Property: Certificate Electricity */
    $overview_handler->display->display_options['fields']['certificate_Electricity']['id'] = 'certificate_Electricity';
    $overview_handler->display->display_options['fields']['certificate_Electricity']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['certificate_Electricity']['field'] = 'certificate_Electricity';
    /* Field: Fortissimmo Property: Condition */
    $overview_handler->display->display_options['fields']['conditionvalue']['id'] = 'conditionvalue';
    $overview_handler->display->display_options['fields']['conditionvalue']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['conditionvalue']['field'] = 'conditionvalue';
    /* Field: Fortissimmo Property: Construction year */
    $overview_handler->display->display_options['fields']['construction_year']['id'] = 'construction_year';
    $overview_handler->display->display_options['fields']['construction_year']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['construction_year']['field'] = 'construction_year';
    /* Field: Fortissimmo Property: Coordinates: latitude */
    $overview_handler->display->display_options['fields']['latitude']['id'] = 'latitude';
    $overview_handler->display->display_options['fields']['latitude']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['latitude']['field'] = 'latitude';
    $overview_handler->display->display_options['fields']['latitude']['precision'] = '0';
    /* Field: Fortissimmo Property: Coordinates: longitude */
    $overview_handler->display->display_options['fields']['longitude']['id'] = 'longitude';
    $overview_handler->display->display_options['fields']['longitude']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['longitude']['field'] = 'longitude';
    $overview_handler->display->display_options['fields']['longitude']['precision'] = '0';
    /* Field: Fortissimmo Property: Date: created in Fortissimmo */
    $overview_handler->display->display_options['fields']['date_created_fortissimmo']['id'] = 'date_created_fortissimmo';
    $overview_handler->display->display_options['fields']['date_created_fortissimmo']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['date_created_fortissimmo']['field'] = 'date_created_fortissimmo';
    $overview_handler->display->display_options['fields']['date_created_fortissimmo']['date_format'] = 'long';
    $overview_handler->display->display_options['fields']['date_created_fortissimmo']['second_date_format'] = 'long';
    /* Field: Fortissimmo Property: Date: first synchronized */
    $overview_handler->display->display_options['fields']['date_created']['id'] = 'date_created';
    $overview_handler->display->display_options['fields']['date_created']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['date_created']['field'] = 'date_created';
    $overview_handler->display->display_options['fields']['date_created']['date_format'] = 'long';
    $overview_handler->display->display_options['fields']['date_created']['second_date_format'] = 'long';
    /* Field: Fortissimmo Property: Date: updated date */
    $overview_handler->display->display_options['fields']['date_changed']['id'] = 'date_changed';
    $overview_handler->display->display_options['fields']['date_changed']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['date_changed']['field'] = 'date_changed';
    $overview_handler->display->display_options['fields']['date_changed']['date_format'] = 'long';
    $overview_handler->display->display_options['fields']['date_changed']['second_date_format'] = 'long';
    /* Field: Fortissimmo Property: Date: updated price */
    $overview_handler->display->display_options['fields']['price_changed']['id'] = 'price_changed';
    $overview_handler->display->display_options['fields']['price_changed']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['price_changed']['field'] = 'price_changed';
    $overview_handler->display->display_options['fields']['price_changed']['date_format'] = 'long';
    $overview_handler->display->display_options['fields']['price_changed']['second_date_format'] = 'long';
    /* Field: Fortissimmo Property: Description */
    $overview_handler->display->display_options['fields']['description']['id'] = 'description';
    $overview_handler->display->display_options['fields']['description']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['description']['field'] = 'description';
    $overview_handler->display->display_options['fields']['description']['description_input_format'] = 'plain_text';
    /* Field: Fortissimmo Property: E-Level */
    $overview_handler->display->display_options['fields']['E_Level']['id'] = 'E_Level';
    $overview_handler->display->display_options['fields']['E_Level']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['E_Level']['field'] = 'E_Level';
    /* Field: Fortissimmo Property: Elevator */
    $overview_handler->display->display_options['fields']['elevator']['id'] = 'elevator';
    $overview_handler->display->display_options['fields']['elevator']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['elevator']['field'] = 'elevator';
    $overview_handler->display->display_options['fields']['elevator']['not'] = 0;
    /* Field: Fortissimmo Property: EP Certificate */
    $overview_handler->display->display_options['fields']['certificate_EP']['id'] = 'certificate_EP';
    $overview_handler->display->display_options['fields']['certificate_EP']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['certificate_EP']['field'] = 'certificate_EP';
    /* Field: Fortissimmo Property: EPC Value */
    $overview_handler->display->display_options['fields']['EPC_value']['id'] = 'EPC_value';
    $overview_handler->display->display_options['fields']['EPC_value']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['EPC_value']['field'] = 'EPC_value';
    /* Field: Fortissimmo Property: Floor */
    $overview_handler->display->display_options['fields']['floor']['id'] = 'floor';
    $overview_handler->display->display_options['fields']['floor']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['floor']['field'] = 'floor';
    /* Field: Fortissimmo Property: Floor ID */
    $overview_handler->display->display_options['fields']['floor_id']['id'] = 'floor_id';
    $overview_handler->display->display_options['fields']['floor_id']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['floor_id']['field'] = 'floor_id';
    /* Field: Fortissimmo Property: Garden direction */
    $overview_handler->display->display_options['fields']['direction_gardenvalue']['id'] = 'direction_gardenvalue';
    $overview_handler->display->display_options['fields']['direction_gardenvalue']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['direction_gardenvalue']['field'] = 'direction_gardenvalue';
    /* Field: Fortissimmo Property: Ground: depth */
    $overview_handler->display->display_options['fields']['depth_ground']['id'] = 'depth_ground';
    $overview_handler->display->display_options['fields']['depth_ground']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['depth_ground']['field'] = 'depth_ground';
    $overview_handler->display->display_options['fields']['depth_ground']['precision'] = '0';
    /* Field: Fortissimmo Property: Ground: width */
    $overview_handler->display->display_options['fields']['width_ground']['id'] = 'width_ground';
    $overview_handler->display->display_options['fields']['width_ground']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['width_ground']['field'] = 'width_ground';
    $overview_handler->display->display_options['fields']['width_ground']['precision'] = '0';
    /* Field: Fortissimmo Property: House: depth */
    $overview_handler->display->display_options['fields']['depth_house']['id'] = 'depth_house';
    $overview_handler->display->display_options['fields']['depth_house']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['depth_house']['field'] = 'depth_house';
    $overview_handler->display->display_options['fields']['depth_house']['precision'] = '0';
    /* Field: Fortissimmo Property: House: width */
    $overview_handler->display->display_options['fields']['width_house']['id'] = 'width_house';
    $overview_handler->display->display_options['fields']['width_house']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['width_house']['field'] = 'width_house';
    $overview_handler->display->display_options['fields']['width_house']['precision'] = '0';
    /* Field: Fortissimmo Property: KI: indexed */
    $overview_handler->display->display_options['fields']['indexed_ki']['id'] = 'indexed_ki';
    $overview_handler->display->display_options['fields']['indexed_ki']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['indexed_ki']['field'] = 'indexed_ki';
    $overview_handler->display->display_options['fields']['indexed_ki']['precision'] = '0';
    /* Field: Fortissimmo Property: KI: non indexed */
    $overview_handler->display->display_options['fields']['non_indexed_ki']['id'] = 'non_indexed_ki';
    $overview_handler->display->display_options['fields']['non_indexed_ki']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['non_indexed_ki']['field'] = 'non_indexed_ki';
    $overview_handler->display->display_options['fields']['non_indexed_ki']['precision'] = '0';
    /* Field: Fortissimmo Property: Language */
    $overview_handler->display->display_options['fields']['taal']['id'] = 'taal';
    $overview_handler->display->display_options['fields']['taal']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['taal']['field'] = 'taal';
    /* Field: Fortissimmo Property: Licenses: Allocation license */
    $overview_handler->display->display_options['fields']['allocation_license']['id'] = 'allocation_license';
    $overview_handler->display->display_options['fields']['allocation_license']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['allocation_license']['field'] = 'allocation_license';
    /* Field: Fortissimmo Property: Licenses: Building license */
    $overview_handler->display->display_options['fields']['building_license']['id'] = 'building_license';
    $overview_handler->display->display_options['fields']['building_license']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['building_license']['field'] = 'building_license';
    /* Field: Fortissimmo Property: Licenses: Presale license */
    $overview_handler->display->display_options['fields']['presale_right']['id'] = 'presale_right';
    $overview_handler->display->display_options['fields']['presale_right']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['presale_right']['field'] = 'presale_right';
    /* Field: Fortissimmo Property: Licenses: Town planning */
    $overview_handler->display->display_options['fields']['town_planningvalue']['id'] = 'town_planningvalue';
    $overview_handler->display->display_options['fields']['town_planningvalue']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['town_planningvalue']['field'] = 'town_planningvalue';
    /* Field: Fortissimmo Property: Licenses: Town planning violation */
    $overview_handler->display->display_options['fields']['town_planning_violation']['id'] = 'town_planning_violation';
    $overview_handler->display->display_options['fields']['town_planning_violation']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['town_planning_violation']['field'] = 'town_planning_violation';
    /* Field: Fortissimmo Property: Livable surface */
    $overview_handler->display->display_options['fields']['area_buildable']['id'] = 'area_buildable';
    $overview_handler->display->display_options['fields']['area_buildable']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['area_buildable']['field'] = 'area_buildable';
    $overview_handler->display->display_options['fields']['area_buildable']['precision'] = '0';
    /* Field: Fortissimmo Property: Main picture */
    $overview_handler->display->display_options['fields']['picture_0_file']['id'] = 'picture_0_file';
    $overview_handler->display->display_options['fields']['picture_0_file']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['picture_0_file']['field'] = 'picture_0_file';
    $overview_handler->display->display_options['fields']['picture_0_file']['fortissimmo_title_as_alt'] = TRUE;
    /* Field: Fortissimmo Property: Number of bedrooms */
    $overview_handler->display->display_options['fields']['Bedrooms']['id'] = 'Bedrooms';
    $overview_handler->display->display_options['fields']['Bedrooms']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['Bedrooms']['field'] = 'Bedrooms';
    /* Field: Fortissimmo Property: Parent property */
    $overview_handler->display->display_options['fields']['ref_parent']['id'] = 'ref_parent';
    $overview_handler->display->display_options['fields']['ref_parent']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['ref_parent']['field'] = 'ref_parent';
    /* Field: Fortissimmo Property: Property price */
    $overview_handler->display->display_options['fields']['price']['id'] = 'price';
    $overview_handler->display->display_options['fields']['price']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['price']['field'] = 'price';
    $overview_handler->display->display_options['fields']['price']['empty_text'] = '';
    /* Field: Fortissimmo Property: Reference */
    $overview_handler->display->display_options['fields']['reference']['id'] = 'reference';
    $overview_handler->display->display_options['fields']['reference']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['reference']['field'] = 'reference';
    /* Field: Fortissimmo Property: Registration cost */
    $overview_handler->display->display_options['fields']['registration_cost']['id'] = 'registration_cost';
    $overview_handler->display->display_options['fields']['registration_cost']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['registration_cost']['field'] = 'registration_cost';
    $overview_handler->display->display_options['fields']['registration_cost']['precision'] = '0';
    /* Field: Fortissimmo Property: Renovation year */
    $overview_handler->display->display_options['fields']['renovation_year']['id'] = 'renovation_year';
    $overview_handler->display->display_options['fields']['renovation_year']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['renovation_year']['field'] = 'renovation_year';
    /* Field: Fortissimmo Property: Title */
    $overview_handler->display->display_options['fields']['title']['id'] = 'title';
    $overview_handler->display->display_options['fields']['title']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['title']['field'] = 'title';
    $overview_handler->display->display_options['fields']['title']['label'] = '';
    $overview_handler->display->display_options['fields']['title']['fortissimmo_title_make_link'] = FALSE;
    $overview_handler->display->display_options['fields']['title']['fortissimmo_title_reference_fallback'] = TRUE;
    /* Field: Fortissimmo Property: Total number of floors */
    $overview_handler->display->display_options['fields']['floors_Total']['id'] = 'floors_Total';
    $overview_handler->display->display_options['fields']['floors_Total']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['floors_Total']['field'] = 'floors_Total';
    /* Field: Fortissimmo Property: Total surface */
    $overview_handler->display->display_options['fields']['area_ground']['id'] = 'area_ground';
    $overview_handler->display->display_options['fields']['area_ground']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['area_ground']['field'] = 'area_ground';
    $overview_handler->display->display_options['fields']['area_ground']['precision'] = '0';
    /* Field: Fortissimmo Property: Transaction */
    $overview_handler->display->display_options['fields']['transaction']['id'] = 'transaction';
    $overview_handler->display->display_options['fields']['transaction']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['transaction']['field'] = 'transaction';
    /* Field: Fortissimmo Property: Type of property */
    $overview_handler->display->display_options['fields']['type']['id'] = 'type';
    $overview_handler->display->display_options['fields']['type']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['fields']['type']['field'] = 'type';
    /* Filter criterion: Fortissimmo Property: Has parent */
    $overview_handler->display->display_options['filters']['ref_parent']['id'] = 'ref_parent';
    $overview_handler->display->display_options['filters']['ref_parent']['table'] = $overview_view->base_table;
    $overview_handler->display->display_options['filters']['ref_parent']['field'] = 'ref_parent';
    $overview_handler->display->display_options['filters']['ref_parent']['value'] = '0';

    $views[$overview_view->name] = $overview_view;
  }

  return $views;
}
