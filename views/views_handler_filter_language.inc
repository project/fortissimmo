<?php

/**
 * @file
 * Filter by language
 */
class views_handler_filter_language extends views_handler_filter_in_operator {

  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Language');
      $options = array(
        '***CURRENT_LANGUAGE***' => t("Current user's language"),
        '***DEFAULT_LANGUAGE***' => t("Default site language"),
        LANGUAGE_NONE => t('No language')
      );

      $languages = fortissimmo_get_property_languages();
      foreach ($languages as $value => $name) {
        $options[$value] = t($name);
      }
      $this->value_options = $options;
    }
  }

}
